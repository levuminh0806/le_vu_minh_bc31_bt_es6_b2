let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let listKinh = () => {
  let contentHTML = "";
  for (let index = 0; index < dataGlasses.length; index++) {
    let kinh = dataGlasses[index];
    contentHTML += `<img  src="${kinh.src}" onclick="kinh('${kinh.virtualImg}')"
      />`;
    let infor = `
      <h5> Brand : ${dataGlasses[index].brand}</h5>
      <h4>Name : ${dataGlasses[index].name}</h4>
      <h4>Color :${dataGlasses[index].color}</h4>
      <h4>Price :${dataGlasses[index].price}</h4>
      <h5>Description : ${dataGlasses[index].description}</h5>
      `;
    // console.log("infor: ", infor);
    document.getElementById("glassesInfo").innerHTML = infor;
  }
  document.querySelector("#vglassesList").innerHTML = contentHTML;
};
listKinh();
window.kinh = (virtualImg) => {
  let anh = `<img id="tatbat"  src="${virtualImg}" />`;
  document.getElementById("avatar").innerHTML = anh;
  document.getElementById("glassesInfo").style.display = "block";
};
window.removeGlasses = (param) => {
  console.log(param);
  if (param === false) {
    document.getElementById("tatbat").style.display = "none";
    document.getElementById("glassesInfo").style.display = "none";
  } else {
    document.getElementById("tatbat").style.display = "block";
    document.getElementById("glassesInfo").style.display = "block";
  }
};
